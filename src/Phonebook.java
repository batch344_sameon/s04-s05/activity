import java.util.ArrayList;
import java.util.Arrays;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook(){
        this.contacts = new ArrayList<Contact>();
    }

    public Phonebook(Contact contact){
        this.contacts = new ArrayList<Contact>(Arrays.asList(contact));
    }

    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }

    public void setContacts(Contact contact){
        this.contacts.add(contact);
    }
}
