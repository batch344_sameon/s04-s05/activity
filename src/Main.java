public class Main {
    public static void main(String[] args) {
        Contact contact1 = new Contact();
        contact1.setName("John Doe");
        contact1.setContactNumber("+639152468596");
        contact1.setAddress("my home in Quezon City");

        Phonebook phonebook = new Phonebook(contact1);

        Contact contact2 = new Contact("Jane Doe", "+639162148573", "my home in Caloocan City");
        phonebook.setContacts(contact2);

        if(phonebook.getContacts().isEmpty()){
            System.out.println("The phonebook is empty.");
        }
        else {
            for(Contact contact : phonebook.getContacts()){
                System.out.println(contact.getName());
                System.out.println("-------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber());
                System.out.println("-------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                System.out.println(contact.getAddress());
                System.out.println("====================================");
            }
        }
    }
}